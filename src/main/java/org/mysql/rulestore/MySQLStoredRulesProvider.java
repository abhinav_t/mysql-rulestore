package org.mysql.rulestore;

import org.mysql.rulestore.config.MySQLConfiguration;
import org.rules.runner.persistedrules.spi.PersistedRulesProvider;

public class MySQLStoredRulesProvider implements PersistedRulesProvider<MySQLConfiguration> {

    private MySQLConfiguration configuration;

    /**
     * Configures the provider so that it can fetch the rules from the particular data-source.
     *
     * @param configuration
     */
    @Override
    public void configure(MySQLConfiguration configuration) {
        this.configuration = configuration;
    }

    @Override
    public String loadRules() {
        return mockDrl();
    }

    private String mockDrl() {
        String drl = "package org.applicant.example.rules;\n\n"
                + "import org.healthmonitor.model.AssessmentInput;\n\n"
                + "global org.healthmonitor.model.ExecutionResult result;\n\n"
                + "dialect \"mvel\"\n\n"
                + "rule \"Health Assessment Score\"\n"
                + "\twhen\n"
                + "\t\tAssessmentInput(cibilScore > 600)\n"
                + "\t\tAssessmentInput(annualCTC > 1200000 && typicalMonthlyExpenses <= 50000 && lifeInsuranceCover >= 4000000)\n"
                + "\tthen\n"
                + "\t\tresult.setScore(85);\n"
                + "\tend\n";

        return drl;
    }

    @Override
    public void updateRules(String rules) {
        // TODO: implement something reasonable here
    }

}
