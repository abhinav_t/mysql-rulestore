package org.mysql.rulestore.config;

import java.util.Objects;

public class MySQLConfiguration {

    public final String jdbcUrl;
    public final String query;

    public MySQLConfiguration(String jdbcUrl, String query) {
        this.jdbcUrl = jdbcUrl;
        this.query = query;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MySQLConfiguration that = (MySQLConfiguration) o;
        return jdbcUrl.equals(that.jdbcUrl) &&
                query.equals(that.query);
    }

    @Override
    public int hashCode() {
        return Objects.hash(jdbcUrl, query);
    }

    @Override
    public String toString() {
        return "MySQLConfiguration{" +
                "jdbcUrl='" + jdbcUrl + '\'' +
                ", query='" + query + '\'' +
                '}';
    }

}
